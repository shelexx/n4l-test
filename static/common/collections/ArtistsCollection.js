define([
    'jquery'
    ,'common/classes/Collection'
    ,'common/models/ArtistModel'
], function ($, Collection, Model) {

    'use strict';

    /** @const */
    var TOP_ARTISTS_URL = app.getApiUrl({method: 'chart.gettopartists'}),
        SEARCH_URL      = app.getApiUrl({method: 'artist.search'});

    /**
     * @class ArtistsCollection
     * @extends Collection
     * */
    var ArtistsCollection = Collection.extend({

        /** @type ArtistModel */
        model: Model

        ,options: {
            limit : 10
        }

        ,constructor: function ArtistsCollection() {
            Collection.apply(this, arguments);
        }

        /**
         * @param {String|Number} [limit]
         * */
        ,getList: function(limit) {

            this.reset();
            $.ajax({
                url: TOP_ARTISTS_URL
                ,data: app.convertObjectToURL({limit: limit || this.options.limit})
                ,success: _.bind(function(data) {
                    if('undefined' !== typeof data['artists'] && 'undefined' !== typeof data['artists']['artist']) {
                        this.add(data['artists']['artist'], {silent: true});
                        this.trigger('list.loaded', this);
                    }
                }, this)
            });

        }

        /**
         * @param {String} request
         * @param {String|Number} [limit]
         * */
        ,searchResult: function(request, limit) {

            this.reset();
            $.ajax({
                url: SEARCH_URL
                ,data: app.convertObjectToURL({limit: limit || this.options.limit, artist: request})
                ,success: _.bind(function(data) {
                    if('undefined' !== typeof data['results'] && 'undefined' !== typeof data['results']['artistmatches']) {
                        this.add(data['results']['artistmatches']['artist'], {silent: true});
                        this.trigger('search.result', this);
                    }
                }, this)
            });

        }

    });

    return ArtistsCollection;

});