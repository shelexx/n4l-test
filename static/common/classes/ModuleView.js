define([
    'jquery'
    ,'underscore'
    ,'classes/View'
], function($, _, View){

    'use strict';

    /**
     * @class ModuleView
     * @extends View
     * */
    var ModuleView = View.extend({

        _template: ''

        ,_hiddenClass: 'hidden'

        ,_loadindClass: 'loading'

        ,_registered: {}

        ,_containerIsReady: false

        ,constructor: function ModuleView(){
            View.apply(this, arguments);

            var selector = _.isFunction(this.selector) ? this.selector(): this.selector;
            if(selector){
                this.setElement($(selector));
            }

            this
                .listenTo(this.model, 'pause', _.bind(function(){
                    if(this.$el){
                        this.$el.addClass(this._hiddenClass);
                    }
                },this))
                .listenTo(this.model, 'resume', _.bind(function(){
                    if(this.$el){
                        this.$el
                            .removeClass(this._hiddenClass)
//                            .removeClass(this._loadindClass)
                        ;
                    }
                }, this))
                .listenTo(this.model, 'module.load', _.bind(function(moduleName){
                    if(this.$el){
//                        this.elementByModuleName(moduleName).addClass(this._loadindClass);
                    }
                }, this))
            ;
        }

        ,showPreloader: function(){
            this.togglePreloader(true);
        }

        ,hidePreloader: function(){
            this.togglePreloader(false);
        }

        ,togglePreloader: function(show){
            if(this.$el){
                this.$el.toggleClass(this._loadindClass, !!show);
            }
        }

        ,show: function(){
            this.toggle(true);
        }

        ,hide: function(){
             this.toggle(false);
        }

        ,toggle: function(show){
            if(this.$el){
                this.$el.toggleClass(this._hiddenClass, !!show);
            }
        }

        ,selector: function(){
            var parent = this.model.get('parent')
                ,result = '[data-module=' + this.model.id + ']';

            if(parent){
                result = parent.getView().$el.find(result);
            }

            return result;
        }

//        ,render: function(){
//            this.$el.html( _.template( this._template ) );
//            return this.$el;
//        }

        ,pauseModules: function(){
            this.$el.find('[data-module]').addClass(this._hiddenClass);
        }

        ,elementByModuleName: function(moduleName){
            var $result = $();
            if(this.$el){
                $result = this.$el.find('[data-module=' + moduleName + ']');
            }
            return $result;
        }

        ,register: function(name, events){
            var _object = null
                ;

            this._registered[name] = {};

            for(var _event in events){
                if(events.hasOwnProperty(_event)){
                    this._registered[name][_event] = null;
                }
            }

            for(var event in events){
                if(events.hasOwnProperty(event)){
                    _object = events[event];

                    this.listenTo(_object, event, _.bind(function(event, data){
                        var reg = this._registered[name]
                            ,result = true
                            ;
                        console.error(event, data);
                        reg[event] = data;
                        for(var evt in reg){
                            if(reg.hasOwnProperty(evt))
                            result = reg[evt] && result;
                        }

                        if(result){
                            this.trigger(name, reg);
                        }

                    }, this, event));
                }
            }
        }
    });

    return ModuleView;
});
