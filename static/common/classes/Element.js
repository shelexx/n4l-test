define([
    'jquery'
    ,'classes/View'
], function ($, View) {

    /** @class Element */
    var Element = View.extend({

        events: {

        }

        ,constructor: function Element() {
            View.apply(this, arguments);
        }

    });

    return Element;

});