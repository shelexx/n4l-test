define([
    'underscore'
    ,'backbone'
], function(_, Backbone){

    /**
     * @class Model
     * @extends Backbone.Model
     * */
    var Model = Backbone.Model.extend({
        constructor: function Model(){
            Backbone.Model.apply(this, arguments);
        }

        ,setDefaults: function() {
            this.attributes = JSON.parse(JSON.stringify(this.defaults));
        }
    });

    return Model;
});
