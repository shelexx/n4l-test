define([
    'jquery'
    ,'underscore'
    ,'backbone'
], function ($, _, Backbone) {

    /**
     * @class View
     * @extends Backbone.View
     * */
    var View = Backbone.View.extend({

        hiddenClass: 'hidden'

        ,method: 'replace'

        ,model: null

        ,_template: null

        ,constructor: function View() {
            Backbone.View.apply(this, arguments);
        }

        /**
         * @param {Object} [data]
         * @param {Object} [options]
         * */
        ,render: function (data, options) {
            this.trigger('beforeRender');
            if(this._template) {

                var result = _.template(this._template, data, options);

                switch(this.method) {
                    case 'append':
                        this.$el.append(result);
                        break;
                    default:
                        this.$el.html(result);
                        break;
                }

                mediator.publish('element', this.$el);
            }

            this.trigger('afterRender', data, options);
            return this.$el;
        }

        ,elementByLabel: function (label) {
            var prefix = '';
            if (this.model) {
                prefix = this.model.id + '.';
            }

            var $el = this.$el.find("[data-label='" + prefix + label + "']:first");

            return $el.length == 0 ? null : $el;
        }

        ,hide: function () {
            this.toggle(true);
        }

        ,show: function () {
            this.toggle(false);
        }

        /**
         * @param {boolean} [status]
         * */
        ,toggle: function (status) {
            if (this.$el) {
                status = typeof status == 'undefined' ? !this.$el.hasClass(this.hiddenClass) : status;
                this.$el.toggleClass(this.hiddenClass, status);
            } else {
                log.w('Element not found for toggle', this);
            }

        }
    });

    return View;
});
