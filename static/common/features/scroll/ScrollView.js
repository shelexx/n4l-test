define([
    'jquery'
    ,'classes/View'
    ,'text!./tmpl/scroll.html'
], function($, View, tmpl){

    tmpl = _.template(tmpl);

    /** @class ScrollView */
    var ScrollView = View.extend({

        $scrollBar: null

        ,$scrollElement: null

        ,$sliderElement: null

        ,sliderHeight: 0

        ,timer: null

        ,events: {
            'click [data-label=scroll]': function(e) {
                this.scroll(e.offsetY > (parseInt(this.$sliderElement.css('top')) + this.sliderHeight / 2) ? -3: 3);
            }
            ,'click [data-label=slider]': function(e) {
                e.stopPropagation();
            }
        }

        ,constructor    : function ScrollView(options) {
            View.apply(this, arguments);


            if(options.$el) {
                this.setElement(options.$el);

                this.$el.css({'position': 'relative', 'overflow': 'hidden'});

                var label = 'scrollable';

                if(!this.elementByLabel('scroll')) {
                    this.$el.append(tmpl());
                    this.$scrollElement = options.$el.children().first();

                    label = this.$scrollElement.attr('data-label') ? this.$scrollElement.attr('data-label'): label;
                    this.$scrollElement.attr('data-label', label).css({'position': 'absolute', 'top': '0px'});

                    mediator.subscribe('window.resize', _.bind(this.onChange, this));
                }

                this.$scrollBar = this.elementByLabel('scroll');
                this.$sliderElement = this.elementByLabel('slider');
                this.sliderHeight = this.$sliderElement.height();

                this.events['DOMSubtreeModified [data-label="' + label + '"]:first'] = this.onChange;
                this.events['mousewheel [data-label="' + label + '"]:first'] = this.onScroll;

                this.delegateEvents();
            }
        }

        ,toggle: function(status) {
            this.$scrollBar.css('transition', status ? '0.2s': '0.8s');
            this.$scrollBar.toggleClass('hover', status);
        }

        ,showTemporary: function() {
            this.toggle(true);
            clearTimeout(this.timer);
            this.timer = setTimeout(_.bind(function() {
                this.toggle(false);
            }, this), 1000);
        }

        ,onChange: function() {
            this.scroll(0);
//            console.error(this.$el.get(0).ownerDocument, this.$el.get(0));
        }

        ,onScroll: function(e) {
            if(this.scroll(e.deltaY)) {
                this.showTemporary();
            }
        }

        ,scroll: function(delta) {

            var lastTop = parseInt(this.$scrollElement.css('top').replace(/px/, ''))
                ,height = this.$scrollElement.get(0).scrollHeight
                ,containerHeight = this.$el.height()
                ,currentTop = lastTop + delta * 20
                ,result
                ;

            if(height > containerHeight) {

                currentTop = Math.abs(currentTop) > (height - containerHeight) ? -1 * (height - containerHeight): (currentTop > 0 ? 0: currentTop);

                this.$sliderElement.css('top', ((containerHeight - this.sliderHeight) * Math.abs(currentTop) / (height - containerHeight)) + 'px');
                this.$scrollElement.css('top', currentTop + 'px');

                result = true;
            } else {
                this.$scrollElement.css('top', '0px');

                result = false;
            }

            return result;
        }

    });

    return ScrollView;

});
