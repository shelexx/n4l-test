define([
    'jquery'
    ,'classes/View'
], function($, View){

    /** @class ScrollView */
    var ScrollView = View.extend({

        events: {
            'DOMSubtreeModified': function() {

                console.error('render', this.$el.height(), this.$el.get(0).scrollHeight);
            }
            ,'DOMNodeRemoved': function() {
                console.error('removed');
            }
        }


        ,constructor    : function ScrollView(options) {
            View.apply(this, arguments);

            if(options.$el) {
                this.setElement(options.$el);
            }
        }

        ,showScroll: function() {

        }

        ,hideScroll: function() {

        }

        ,render: function() {



        }
    });

    return ScrollView;

});
