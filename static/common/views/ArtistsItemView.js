define([
    'jquery'
    ,'underscore'
    ,'common/classes/View'
    ,'text!../tmpl/list_item.html'
], function($, _, View, tmpl){

    var _this = {
        bindEvents: function(){

        }
    };

    /**
     * @class TopartistsItemView
     * @extends View
     * */
    var TopartistsItemView = View.extend({

        _template: tmpl

        ,tagName : 'li'

        ,events: {

        }

        ,constructor: function ArtistsItemView(){
            View.apply(this, arguments);
        }

        ,initialize: function(){
            _this.bindEvents.call(this);
        }

    });

    return TopartistsItemView;

});