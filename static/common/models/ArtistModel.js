define([
    '../classes/Model'
], function (Model) {

    'use strict';

    /** @const */
    var FULL_INFO_URL = app.getApiUrl({method: 'artist.getinfo'});

    /**
     * @class ArtistModel
     * @extends Model */
    var ArtistModel = Model.extend({

        defaults: {
            name        : ''
            ,playcount  : ''
            ,listeners  : ''
            ,mbid       : ''
            ,url        : ''
            ,streamable : true
            ,image      : []
            ,bandmembers: {}
            ,bio        : {content : ''}
            ,ontour     : "0"
            ,similar    : {}
            ,stats      : {listeners: "0", playcount: "0"}
            ,tags       : {}
        }

        ,constructor: function ArtistModel() {
            Model.apply(this, arguments);
        }

        /**
         * @param {String} name
         * */
        ,getInfo: function(name) {

            $.ajax({
                url: FULL_INFO_URL
                ,data: app.convertObjectToURL({artist: name})
                ,success: _.bind(function(data) {
                    if('undefined' !== typeof data['artist']) {
                        this.set(data['artist']);
                        this.trigger('artist.loaded', this);
                    }
                }, this)
            });
        }

    });

    return ArtistModel;

});