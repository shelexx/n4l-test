define({
    "registration": "регистрация"
    ,"username": "имя пользователя"
    ,"password": "пароль"
    ,"user": "пользователь"
    ,"save": "сохранить"
    ,"edit": "редактировать"
    ,"enter": "войти"
    ,"create": "создать"
    ,"close": "закрыть"
    ,"delete": "удалить"
    ,"view": "посмотреть"
    ,"owner": "создал"
    ,"text": "текст"
    ,"description": "описание"
    ,"title": "заголовок"
    ,"performer": "исполнитель"
    ,"taskType": "тип задания"
    ,"createTask": "Создать задачу"

    ,"created": "дата создания"
    ,"modified": "дата изменения"

    ,"newTask": "Новая задача"
    ,"editTask": "Изменение задачи"

    ,"noTitle": "Без заголовка"
    ,"noDescription": "Без описания"

    ,"confirmDeleteTask": "Удалить задание?"

    ,"notSpecified": "не указан"

    ,"type.task": "задача"
    ,"type.bug": "bug"
    ,"type.feature": "feature"

    ,"status.open": "открыта"
    ,"status.in progress": "в процессе"
    ,"status.reopened": "переоткрыта"
    ,"status.closed": "закрыта"
    ,"status.done": "завершена"


});