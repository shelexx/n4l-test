define({
    "root": {
        "registration": "registration"
        ,"username": "username"
        ,"password": "password"
        ,"user": "user"
        ,"save": "save"
        ,"edit": "edit"
        ,"enter": "enter"
        ,"close": "close"
        ,"create": "create"
        ,"delete": "delete"
        ,"view": "view"
        ,"owner": "owner"
        ,"text": "text"
        ,"description": "description"
        ,"title": "title"
        ,"performer": "performer"
        ,"taskType": "type"
        ,"createTask": "Create issue"

        ,"created": "created"
        ,"modified": "modified"

        ,"newTask": "Task creating"
        ,"editTask": "Task editing"

        ,"noTitle": "No title"
        ,"noDescription": "No description"



        ,"confirmDeleteTask": "Confirm task delete"

        ,"notSpecified": "not specified"

        ,"type.task": "task"
        ,"type.bug": "bug"
        ,"type.feature": "feature"

        ,"status.open": "open"
        ,"status.in progress": "in progress"
        ,"status.reopened": "reopened"
        ,"status.closed": "closed"
        ,"status.done": "done"


    },"ru-ru": true
});