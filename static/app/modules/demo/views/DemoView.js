define([
    'jquery'
    ,'underscore'
    ,'app/appclasses/FormView'
    ,'text!../tmpl/demo.html'
], function($, _, FormView, tmpl){

    var _private = {
        bindEvents: function(){

        }

        ,formSelectKeyPress: function(evt) {

        }
    };

    /** @class DemoView */
    var DemoView = FormView.extend({

        _template: tmpl

        ,events: {

        }

        ,constructor: function DemoView(){
            FormView.apply(this, arguments);
        }

        ,initialize: function(){
            _private.bindEvents.call(this);
        }

    });

    return DemoView;

});