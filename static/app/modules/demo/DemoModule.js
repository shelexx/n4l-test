define([
    'jquery'
    ,'app/appclasses/FormModule'
    ,'demo/views/DemoView'

], function($, FormModule, View, UserModel){

    'use strict';

    /** @class DemoModule */
    var DemoModule = FormModule.extend({

        constructor: function DemoModule() {
            FormModule.apply(this, arguments);
            this._viewClass = View;
        }

    });

    return DemoModule;

});