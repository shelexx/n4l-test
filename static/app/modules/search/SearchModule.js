define([
    'jquery'
    ,'common/classes/Module'
    ,'./views/SearchView'
    ,'common/models/ArtistModel'
    ,'common/collections/ArtistsCollection'
], function($, Module, View, ArtistModel, ArtistsCollection){

    'use strict';

    /** @private */
    var _this = {

        /** @type ArtistModel */
        model: null

        /** @type ArtistsCollection */
        ,collection: null

        ,bindEvents: function(){ }

        ,onPageLoad: function(request) {

            _this.collection = new ArtistsCollection();
            this.listenTo(_this.collection, 'search.result', _.bind(function() {
                mediator.publish('search.result', _this.collection);
            }, this));

            _this.collection.searchResult(request);

        }

    };

    /** @class SearchModule */
    var SearchModule = Module.extend({

        constructor: function SearchModule() {
            Module.apply(this, arguments);
            this._viewClass = View;

            this.routes = {
                'search(/:request)': {
                    callback: _.bind(_this.onPageLoad, this)
                }
            };
            app.getRouter().addRoutes(this.routes);

        }

    });

    return SearchModule;

});