define([
    'jquery'
    ,'underscore'
    ,'common/classes/ModuleView'
    ,'common/views/ArtistsItemView'
    ,'text!../tmpl/search.html'
], function($, _, ModuleView, ArtistsItemView, tmpl){

    'use strict';

    /** @private */
    var _this = {
        bindEvents: function(){
            mediator.subscribe('search.result', _.bind(this.renderResult, this));
        }

    };

    /**
     * @class SearchView
     * @extends ModuleView
     * */
    var SearchView = ModuleView.extend({

        _template: tmpl

        ,events: {

        }

        ,constructor: function SearchView(){
            ModuleView.apply(this, arguments);
        }

        ,initialize: function(){
            _this.bindEvents.call(this);
        }

        ,renderResult: function(list) {
            var $el = this.elementByLabel('result')
                ,$noresult = this.elementByLabel('noresult')
                ;

            $noresult.addClass('hidden');
            $el.html('');

            if(list.length > 0) {

                list.each(function(model) {
                    $el.append((new ArtistsItemView()).render(model.toJSON()));
                });

            } else {
                $noresult.removeClass('hidden');
            }

        }

    });

    return SearchView;

});