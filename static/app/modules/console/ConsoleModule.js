define('console', [
    'jquery'
    ,'classes/Module'
    ,'comments/views/CommentsView'
    ,'comments/collections/CommentsCollection'
    ,'comments/models/CommentsModel'
], function($, Module, ViewClass, CommentsCollection, CommentsModel){

    'use strict';

    var _private = {
        items: null

        ,bindEvents: function(){

        }
    };

    /**
     * @class ConsoleModule
     * */
    var ConsoleModule = Module.extend({

        constructor: function ConsoleModule() {
            Module.apply(this, arguments);

            this._viewClass = ViewClass;
        }

        ,onRun: function(){
            _private.bindEvents.call(this);
        }

        ,onResume: function(){

        }

    });

    return ConsoleModule;

});