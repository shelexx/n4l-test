define([
    'jquery'
    ,'underscore'
    ,'common/classes/ModuleView'
    ,'common/views/ArtistsItemView'
    ,'text!../tmpl/top.html'
], function($, _, ModuleView, TopartistsItemView, tmpl){

    var _this = {
        onSearch: function() {
            var $el = this.$el.find('[name=search]');

            var val = $el.val();
            if(val && val.length >= 3) {
                val = encodeURIComponent(val);
                app.changeUrl('/search/' + val);
            } else {
                console.error('> 3');
            }

        }

        ,onEnterPress: function(e) {
            if(e.keyCode == 13) {
                _this.onSearch.call(this);
            }
        }
    };

    /**
     * @class TopartistsView
     * @extends ModuleView
     * */
    var TopartistsView = ModuleView.extend({

        _template: tmpl

        ,events: {
            'click .btn-search'     : _this.onSearch,
            'keydown [name=search]' : _this.onEnterPress
        }

        ,constructor: function TopartistsView(){
            ModuleView.apply(this, arguments);
        }

        ,initialize: function(){

        }

        /**
         * @param {ArtistsCollection} collection
         * */
        ,onListReceived: function(collection) {
            var view
                ,$el = this.elementByLabel('list')
                ;

            $el.html('');

            collection.each(function(model) {
                view = new TopartistsItemView();
                $el.append(view.render(model.toJSON()));
            });
        }

    });

    return TopartistsView;

});