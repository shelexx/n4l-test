define([
    'jquery'
    ,'common/classes/Module'
    ,'./views/TopartistsView'
    ,'common/collections/ArtistsCollection'
], function($, Module, View, ArtistsCollection){

    'use strict';

    /** @const */
    var LIMIT = 10;

    var _this = {

        collection: null

    };

    /**
     * @class TopartistsModule
     * @extends Module
     * */
    var TopartistsModule = Module.extend({

        /** @type TopartistsView */
        _view: null

        ,constructor: function TopartistsModule() {
            Module.apply(this, arguments);
            this._viewClass = View;
        }

        ,onRun: function() {
            if(!_this.collection) {
                _this.collection = new ArtistsCollection({limit: LIMIT});

                this._view.listenTo(_this.collection, 'list.loaded', this._view.onListReceived);

                _this.collection.getList();
            }
        }

    });

    return TopartistsModule;

});