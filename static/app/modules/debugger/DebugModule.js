define('debug', [
    'underscore',
    'jquery',
    'classes/Module'
], function(_, $, Module){


    /**
     * @class DebugModule
     * */
    var DebugModule = Module.extend({
        constructor: function DebugModule() {
            Module.apply(this, arguments);
        },
        initialize: function(){

        },

        log: log,

        error: error,

        warn: warn,

        color: color
    });

    return DebugModule;
});