define([
    'jquery',
    'underscore',
    'classes/ModuleView',

    'text!../tmpl/bar.html'
], function($, _, ModuleView, tmpl){

    var _this = {
        bindEvents: function(){

        }
    };

    /** @class BarView */
    var BarView = ModuleView.extend({

        _template: tmpl

        ,el: $('[data-module=bar]')

        ,events: {

        }

        ,constructor: function BarView(){
            ModuleView.apply(this, arguments);
        },

        initialize: function(){
            _this.bindEvents.call(this);
        }

//        ,render: function(){
//            this.$el.html( _.template( tmpl, this.model.toJSON() ) );
//            return this.$el;
//        }

    });

    return BarView;

});