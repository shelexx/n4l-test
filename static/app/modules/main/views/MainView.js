define([
    'jquery'
    ,'underscore'
    ,'common/classes/ModuleView'
    ,'text!../tmpl/main.html'
], function($, _, ModuleView, tmpl){

    var _this = {
        bindEvents: function(){

        }
    };

    /** @class MainView */
    var MainView = ModuleView.extend({

        _template: tmpl

        ,events: {

        }

        ,constructor: function MainView(){
            ModuleView.apply(this, arguments);
        }

        ,initialize: function(){
            _this.bindEvents.call(this);

        }

    });

    return MainView;

});