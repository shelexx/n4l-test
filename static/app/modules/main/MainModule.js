define([
    'jquery'
    ,'common/classes/Module'
    ,'main/views/MainView'
], function($, Module, View){

    'use strict';

    /** @class MainModule */
    var MainModule = Module.extend({

        constructor: function MainModule() {
            Module.apply(this, arguments);
            this._viewClass = View;
        }

        ,onRun: function() {
            this.attachModules('topartists');
        }

    });

    return MainModule;

});