define([
    'jquery'
    ,'common/classes/Module'
    ,'./views/DetailsView'
    ,'common/models/ArtistModel'
    ,'common/collections/ArtistsCollection'
], function($, Module, View, ArtistModel, ArtistsCollection){

    'use strict';

    /** @private */
    var _this = {

        /** @type ArtistModel */
        model: null

        /** @type ArtistsCollection */
        ,collection: null

        ,bindEvents: function(){ }

        ,onPageLoad: function(id) {

            _this.model = new ArtistModel();
            this.listenTo(_this.model, 'artist.loaded', _.bind(function() {
                mediator.publish('details.loaded', _this.model);
            }, this));

            _this.model.getInfo(id);

        }

    };

    /** @class DetailsModule */
    var DetailsModule = Module.extend({

        constructor: function DetailsModule() {
            Module.apply(this, arguments);
            this._viewClass = View;

            this.routes = {
                'details(/:id)': {
                    callback: _.bind(_this.onPageLoad, this)
                }
            };
            app.getRouter().addRoutes(this.routes);

        }

    });

    return DetailsModule;

});