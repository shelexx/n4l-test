define([
    'jquery'
    ,'underscore'
    ,'common/classes/ModuleView'
    ,'common/views/ArtistsItemView'
    ,'text!../tmpl/details.html'
], function($, _, ModuleView, TopartistsItemView, tmpl){

    'use strict';

    /** @private */
    var _this = {
        bindEvents: function(){
            mediator.subscribe('details.loaded', _.bind(this.renderResult, this));
        }

    };

    /**
     * @class DetailsView
     * @extends ModuleView
     * */
    var DetailsView = ModuleView.extend({

        _template: tmpl

        ,events: {

        }

        ,constructor: function DetailsView(){
            ModuleView.apply(this, arguments);
        }

        ,initialize: function(){
            _this.bindEvents.call(this);
        }

        ,render: function() {

        }

        ,renderResult: function(model) {
            log.i('model', model.toJSON());
            this.$el.html(_.template(this._template, model.toJSON()));
            this.renderSimilar(model.get('similar')['artist']);
        }

        ,renderSimilar: function(list) {
            var $el = this.elementByLabel('similar');
            $el.html('');

            _.each(list, function(data) {
                $el.append((new TopartistsItemView()).render(data));
            });
        }

    });

    return DetailsView;

});