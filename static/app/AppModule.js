define([
    'jquery'
    ,'underscore'
    ,'router'
    ,'storage'
    ,'classes/Module'
    ,'app/views/AppView'
], function ($, _, Router, StorageModel, Module, AppView) {

    'use strict';

//    var WSLINK = "localhost:8080";
    /** @const */
    var WSLINK = "node:8080";

    var _config = {
        debug: true, pageCache: 5,

        pages: {
            'top'       : 'top',
            'details'   : 'details',
            'search'    : 'search',
            'demo'      : 'demo'
        }

        ,require: {
            packages: [
                {name: 'top', main: 'TopartistsModule', location: '/app/modules/topartists'}
                ,{name: 'details', main: 'DetailsModule', location: '/app/modules/details'}
                ,{name: 'search', main: 'SearchModule', location: '/app/modules/search'}
                ,{name: 'main', main: 'MainModule', location: '/app/modules/main'}
                ,{name: 'bar', main: 'BarModule', location: '/app/modules/bar'}
                ,{name: 'demo', main: 'DemoModule', location: '/app/modules/demo'}
            ]
        }
    };

    require.config(_config.require);

    var _pages = [];

    var _this = {
        bindEvents: function () {
            this.on('navigate', _.bind(_this.onNavigate, this));

        }

        ,onNavigate: function (link) {
            _router.go(link, true);
        }

        ,socketOnOpen: function(event) {
//            console.error('Открыто', event);
        }

        ,socketOnClose: function(event) {
            if (event.wasClean) {
                console.error('Соединение закрыто чисто');
            } else {
                console.error('Обрыв соединения'); // например, "убит" процесс сервера
            }
            console.error('Код: ', event.code, ' причина: ', event.reason);
        }

        ,socketOnMessage: function(event) {
            var data = JSON.parse(event.data);

            if(_requests[data._key]) {
                var callback = _requests[data._key];
                delete _requests[data._key];
                callback(data.data);
            } else {
                mediator.publish('socket.' + data.command, data.data);
            }
        }

        ,socketOnError: function(error) {
            console.error("Ошибка ", error.message);
        }
    };

    //set ajax defaults
    $.ajaxSetup({
        dataType       : 'json'
        ,processData    : false
        ,traditional    : true
        ,xhrFields      : {
            withCredentials: false
        }
        ,statusCode     : {
            200: function (data) {

            }
        }
    });

    var _router = Router
        ,_storage   = null
        ,_baseUrl   = 'http://ws.audioscrobbler.com/2.0/?api_key=af63054c5a4b43180f139750bd47481c&format=json&'
        ,_socket    = null
        ,_requests  = {}
        ;

    /**
     * @class AppModule
     * @extends Module
     * */
    var AppModule = Module.extend({

        constructor: function AppModule() {
            Module.apply(this, arguments);
        }

        ,onInitialize: function () {

//            _socket = new WebSocket("ws://" + WSLINK);
//            _socket.onopen = _.bind(_this.socketOnOpen, this);
//            _socket.onclose = _.bind(_this.socketOnClose, this);
//            _socket.onerror = _.bind(_this.socketOnError, this);
//            _socket.onmessage = _.bind(_this.socketOnMessage, this);

            _router.initialize();
            _storage = new StorageModel();

            this.modules = {
                bar         : null
                ,main       : null
                ,details    : null
            };

//            this.loadModules('user', _.bind(function () {
//                this.modules['user'].run();
//            }, this));
        }

        ,onRun: function () {
            _this.bindEvents.apply(this);

            /** @type AppView */
            this._view = new AppView({model: this});
            this._view.render();
            this.setReady(true);

            this.routes = {
                '(*actions)': {
                    callback: _.bind(function (link) {

                        var path = _router.getPathParams(link),
                            moduleName = path === '' ? _config.pages[_.keys(_config.pages)[0]] : (_config.pages[path]);

                        if (typeof moduleName == 'undefined') {
                            console.error(404);
                            return false;
                        }

                        this.attachModules(moduleName);

                        return true;
                    }, this)

                }
            };

            app.getRouter().addRoutes(this.routes);

            this.attachModules('bar');
        }

        ,onChildModuleLoad: function (module) {

            if (_.indexOf(this.getPages(), module) != -1) {
                var lastModule = _.last(_pages);

                if (lastModule != module) {

                    _pages = _.without(_pages, module);
                    _pages.push(module);

                    if (_pages.length > _config.pageCache) {
                        var oldModuleName = _pages.shift();
                        this.trigger('module.destroy', oldModuleName);
                        this.modules[oldModuleName].destroy();
                        delete this.modules[oldModuleName];
                    }

                    if (lastModule) {
                        this.trigger('module.pause', lastModule);
                        this.modules[lastModule].pause();
                    }
                }
            }

        }

        ,getPages: function () {
            return _.values(_config.pages);
        }

        ,getRouter: function () {
            return _router;
        }

        ,getStorage: function () {
            return _storage;
        }

        ,getSocket: function() {
            return _socket;
        }

        /**
         * @param {String} name
         * @param {String} [caseType]
         * @return {String}
         * */
        ,text: function (name, caseType) {
            return this._view.translate(name, caseType);
        }

        /**
         * Base url for all ajax calls
         * @return {String}
         */
        ,getBaseUrl: function () {
            return _baseUrl;
        }

        /**
         * @param {Object} [params]
         * @return {String}
         * */
        ,getApiUrl: function (params) {
            return _baseUrl + this.convertObjectToURL(params);
        }

        /**
         * @param {Object} [params]
         * @return {String}
         * */
        ,convertObjectToURL: function(params){
            if(params) {
                var result = [];
                _.each(params, function(value, key) {
                    result.push(key + '=' + value);
                });
                return result.join('&');
            }

            return '';
        }

        ,send: function(command, data, callback) {

            if(_.isFunction(callback)) {
                data._key = Math.random();
                _requests[data._key] = callback;
            }
            this.getSocket().send(JSON.stringify({command: command, data: data}));
        }

        ,changeUrl: function (url) {
            this.trigger('navigate', url);
        }

    });

    return AppModule;

});