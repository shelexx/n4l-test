define([
    'jquery'
    ,'underscore'
    ,'bootstrap'
    ,'classes/ModuleView'

    ,'i18n!app/nls/app'

    ,'text!../tmpl/app.html'
    ,'text!../tmpl/module.html'
    ,'css!/app/assets/app.css'

], function($, _, bootstrap, ModuleView, i18n, tmpl, tmplModule){

    var _resizeTimer;

    var _this = {

        bindEvents: function(){
            this.model.on('module.load module.resume', _.bind(_this.modulePrepare, this));

            this.model.on('locale', _this.localeChange);

            $(window).on('resize', function(e) {
                mediator.publish('window.resize', e.target.innerWidth, e.target.innerHeight);

                clearTimeout(_resizeTimer);
                _resizeTimer = setTimeout(function() {
                    mediator.publish('window.resize.stop', window.innerWidth, window.innerHeight);
                }, 200);
            });
        }

        ,localeChange: function(locale){
            LOCALE = locale;
            setCookie('locale', LOCALE);
        }

        ,onLinkClick: function(e){

            var $el         = $(e.currentTarget)
                ,action     = $el.attr('data-action') ? $el.attr('data-action'): 'link'
                ,href       = $el.attr('href')
                ,protocol   = e.target.protocol + '//'
                ;

            switch(action.toLowerCase()){
                case 'link':
                    if (href.slice(protocol.length) !== protocol) {
                        e.preventDefault();
                        this.model.trigger('navigate', $el.attr('href'), $el);
                    }
                break;
                default:
                    e.preventDefault();
                break;
            }
        }

        ,onKeyDown: function(e) {
            mediator.publish('keydown', e);
        }

        ,onKeyUp: function(e) {
            mediator.publish('keyup', e);
        }

        ,modulePrepare: function(moduleName) {

            var $outerModule = this.$el.find('[data-module=' + moduleName + ']').not(this.$pages.find('[data-module] [data-module]')),
                $innerModule = this.$pages.children('[data-module=' + moduleName + ']');

            if($outerModule.length > 0 && $innerModule.length == 0){

            } else {

                if($innerModule.length == 0){
                    this.$pages.append(_.template(tmplModule, {'id': moduleName}));
                    $innerModule = this.$pages.children('[data-module=' + moduleName + ']');
                }

                $innerModule.removeClass(this._hiddenClass);

            }
        }

        ,onSubmit: function(e){
            e.preventDefault();
        }

    };

    /**
     * @class AppView
     * @extends ModuleView
     * */
    var AppView = ModuleView.extend({

        _template: tmpl

        ,$pages: null

        ,events: {
            'click a:not([data-bypass])': _this.onLinkClick
            ,'submit': _this.onSubmit
        }

        ,constructor: function AppView(){
            ModuleView.apply(this, arguments);
        }

        ,initialize: function(){
            _this.bindEvents.call(this);

            this.listenTo(this, 'afterRender', _.bind(function(){
                this.$pages = this.elementByLabel('pages');
                window.onkeydown = _this.onKeyDown;
                window.onkeyup = _this.onKeyUp;

            }, this));

            mediator.subscribe('element', function($el) {

                var $selects = $el.find('select[data-type="form-select"]');

                if($selects.length > 0) {
                    requirejs(['common/elements/SelectElement'], function(SelectElement) {
                        _.each($selects, function(element) {
                            new SelectElement({element: element});
                        });
                    });
                }
            });
        }

        ,translate: function(name, caseType) {

            var text = typeof i18n[name] == 'undefined' ? name : i18n[name];
            caseType = typeof caseType == 'undefined' ? '': caseType;

            switch(caseType) {
                case 'l':
                    text = text.toLowerCase();
                    break;
                case 'f':
                    text = text.ucFirst();
                    break;
                case 'u':
                    text = text.toUpperCase();
                    break;
            }

            return text;
        }

    });

    return AppView;

});