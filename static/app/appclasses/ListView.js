define([
    'underscore',
    'classes/ModuleView'
], function(_, ModuleView){

    /** @class ListView */
    var ListView = ModuleView.extend({

        _itemViewClass  : null

        ,_template      : null

        ,_$list         : null

        ,constructor    : function ListView(){
            ModuleView.apply(this, arguments);
        }

        ,getList        : function(){
            if(!this._$list) {
                this._$list = this.elementByLabel('list');
            }
            return this._$list;
        }

        ,render         : function(){
            this.getList().html('');
        }

        ,addItem        : function(model){
            var $list       = this.getList()
                ,itemView   = new this._itemViewClass({model: model})
                ;

            $list.append(itemView.render());
        }

        ,renderList     : function(collection){
            var $list       = this.getList()
                ;

            $list.html('');
            collection.each(_.bind(this.addItem, this));
        }

    });

    return ListView;

});
