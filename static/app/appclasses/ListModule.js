define([
    'underscore',
    'classes/Module'
], function(_, Module){

    var _private = {

        fetch: function(){
            if(this._collection){
                this._collection.fetch({silent: true});
                //JSON.stringify({data: this._filter})
            }
        }

        ,onLoadItem: function(){
            log.c('onLoadItem', arguments);
            this._view.renderList(this._collection, this._currentItem);
        }

    };


    /** @class ListModule */
    var ListModule = Module.extend({

        _collection: null

        ,_currentItem: null

        ,_filter: {
            page: 0
        }

        ,constructor: function ListModule(){
            Module.apply(this, arguments);
        }

        ,initialize: function(){
            Module.prototype.initialize.apply(this, arguments);
        }

        ,run: function(){
            Module.prototype.run.apply(this, arguments);

            if(this._collection){
                this.listenTo(this._collection, 'sync', function(){
                    this._view.renderList(this._collection);
                });
                this.listenTo(this._collection, 'add', function(model){
                    this._view.addItem(model);
                });
            }

            _private.fetch.call(this);
        }

        /**
         * @param {Number}
         * */
        ,removeItem: function(id) {
            if(this._collection) {
                this._collection.remove(id);
            }
        }

    });

    return ListModule;

});
