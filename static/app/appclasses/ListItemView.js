define([
    'underscore',
    'classes/View'
], function(_, View){

    /** @class ListItemView */
    var ListItemView = View.extend({

        constructor: function ListItemView(){
            View.apply(this, arguments);
        }

        ,render: function() {

        }
    });

    return ListItemView;

});
