define([
    'underscore',
    'classes/Module'
], function (_, Module) {

    var _private = {
        bindEvents: function () {
            this.listenTo(this, 'error', this.onError);
            this.listenTo(this, 'formdata', this.onData);
        }
    };

    /** @class FormModule */
    var FormModule = Module.extend({

        model: null

        ,constructor: function FormModule(options) {
            Module.apply(this, arguments);
        }

        ,getModel: function () {
            return this.model;
        }

        ,run: function () {
            Module.prototype.run.apply(this, arguments);
            _private.bindEvents.call(this);
        }

        ,onError: function (model, xhr) {
            log.e('ERROR', JSON.parse(xhr.responseText)['error']);
        }

        ,onAfterCreate: function() {}

        ,onAfterSave: function() {}

        ,onData: function (data) {
            //Hack for correct post
            if (!data[this.model.idAttribute]) {
                data[this.model.idAttribute] = null;
            }

            this.model.set(data);
            this.onSubmit();
        }

        ,onSubmit: function() {
            var action = this.model.get('id') ? 'edit': 'add';
            this.model.save({}, {
                'success': _.bind(function(action){
                    if(action == 'add') {
                        this.onAfterCreate();
                    } else {
                        this.onAfterSave();
                    }
                }, this, action)
            });

        }
    });

    return FormModule;
});
