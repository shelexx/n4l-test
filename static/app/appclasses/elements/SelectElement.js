define([
    'underscore'
    ,'app/appclasses/Element'
    ,'text!./tmpl/select.html'
], function (_, Element, tmpl) {

    'use strict';

    var _private = {

        formSelectChoise: function(evt) {
            log.c('formSelectChoise');
            evt.stopPropagation();

            this.$inputValue.val(evt.target.getAttribute('data-value'));
            this.$inputFocus.val(evt.target.innerHTML);

            this.$el.removeClass('open');

            window.setTimeout(_.bind(function() {
                this.$inputValue.trigger('change');
            }, this), 0);
        }

        ,onClick: function(evt) {
            if($(evt.target).attr('type') != 'hidden') {
                this.$el.toggleClass('open');
            }
        }

        ,onBlur: function() {
            if(this.$el.hasClass('open')) {

                this.$el.removeClass('open');

                window.setTimeout(_.bind(function() {
                    this.$el.focus();
                }, this), 0);
            }
        }
    };

    /** @class SelectElement */
    var SelectElement = Element.extend({

        $inputValue: null

        ,$inputFocus: null

        ,events: {
            'click li'      : _private.formSelectChoise
            ,'click'        : _private.onClick
            ,'blur'         : _private.onBlur
        }

        ,constructor: function SelectElement(options) {
            Element.apply(this, arguments);



            var $el = $(options.element);

            this.setElement($(_.template(tmpl, {$el: $el, $first: $el.find('option:first'), $selected: $el.find('[selected]')})));
            $el.replaceWith(this.$el);

//            this.$el.on('click label', function(evt){
//                log.c('one more', evt.target);
//            });

            this.$inputValue = this.$el.find('[type=hidden]');
            this.$inputFocus = this.$el.find('[disabled]');
        }

    });

    return SelectElement;

});