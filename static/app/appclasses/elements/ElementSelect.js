define([
    'underscore'
    ,'app/appclasses/Element'
], function (_, Element) {

    /** @class Element */
    var SelectElement = Element.extend({

        events: {

        }

        ,constructor: function SelectElement() {
            Element.apply(this, arguments);
        }

    });

    return SelectElement;

});