define([
    'underscore',
    'classes/Module'
], function(_, Module){

    /** @class PrivateModule */
    var PrivateModule = Module.extend({
        constructor: function PrivateModule(){
            Module.apply(this, arguments);
        }


    });

    return PrivateModule;

});
