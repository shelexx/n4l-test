define([
    'jquery'
    ,'classes/View'
], function ($, View) {

    /** @class ModalView */
    var ModalView = View.extend({

        constructor: function View() {
            View.apply(this, arguments);
        }

        ,show: function(html) {
            $(tmpl).modal();
        }

        ,confirm: function() {

        }

        ,hide: function() {

        }

    });

    return ModalView;

});