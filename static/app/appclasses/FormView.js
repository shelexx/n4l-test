define([
    'jquery'
    , 'underscore'
    , 'classes/ModuleView'
], function ($, _, ModuleView) {

    var _private = {
        getFormData: function () {
            var $form = $(this.$el.find('form').get(0))
                , elements = $form.find('input[name]:not([data-ignore=true]), textarea[name]:not([data-ignore=true]), select[name]:not([data-ignore=true])')
                , $element = null
                , result = {}
                ;

            _.each(elements, _.bind(function (element) {
                $element = $(element);
                result[$element.attr('name')] = $element.val();
            }, this));

            return result;
        }

        ,onSubmit: function (e) {
            e.preventDefault();
            this.model.trigger('formdata', this.getFormData());
        }
    };

    /** @class FormView */
    var FormView = ModuleView.extend({

        events: {
            'submit form': _private.onSubmit
            ,'click [data-type=submit]': _private.onSubmit
        }

        ,constructor: function FormView() {
            ModuleView.apply(this, arguments);
        }

        ,getFormData: function () {
            return _private.getFormData.call(this);
        }

    });

    return FormView;

});