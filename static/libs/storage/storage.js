define([
    'underscore'
    ,'classes/Model'
], function(_, Model){

    'use strict';

    /** @const */
    var STORAGE_NAME = 'storage';

    var _this = {

        /** @private */
        getData: function() {
            return JSON.parse(localStorage[STORAGE_NAME]);
        },

        /** @private */
        setData: function(data) {
            localStorage[STORAGE_NAME] = JSON.stringify(data);
        }

    };

    /**
     * @class StorageModel
     * @extends Model
     * */
    var StorageModel = Model.extend({

        /** @private */
        support: true

        /** @constructor */
        ,constructor: function Storage() {
            Model.apply(this, arguments);

            if(!localStorage) {
                this.support = false;
            } else {
                if(!localStorage[STORAGE_NAME]) {
                    localStorage[STORAGE_NAME] = '{}';
                }
            }

        }

        /**
         * Get storage values
         * @param {String} path
         * @param {String|Object} [def] Default value
         * */
        ,getData: function(path, def) {

            var args = path.split(/\./),
                currentValue = _this.getData();

            _.each(args, function(arg) {
                if(currentValue && currentValue[arg]) {
                    currentValue = currentValue[arg];
                } else {
                    currentValue = null;
                }
            });

            if(def && !currentValue) {
                currentValue = def;
            }

            return currentValue;
        }
        /**
         * Set to the storage value by the module path
         * @param {String} path
         * @param {Object} value
         * */
        ,setData: function(path, value) {

            var args = path.split(/\./),
                data = _this.getData(),
                currentValue = data;

            _.each(args, function(arg, index) {

                if(index < args.length - 1) {
                    if('undefined' == typeof currentValue[arg]) {
                        currentValue[arg] = {};
                    }

                    currentValue = currentValue[arg];
                } else {
                    currentValue[arg] = value;
                }

            });

            _this.setData(data);
        }

    });

    return StorageModel;
});
