define([
    'jquery'
    ,'underscore'
], function($, _){

    if(!String.prototype.ucFirst) String.prototype.ucFirst = function() {
        return this.charAt(0).toUpperCase() + this.substr(1);
    };

    /** @param {...*} message */
    var i = function(message){};

    window.log = { i: i, c: i, w: i, e: i};

    if(DEBUG && typeof console.log != "object"){
        
        log.i = typeof console.log != 'undefined' ? _.bind(console.log, console): i;
        log.c = typeof console.error != 'undefined' ? _.bind(console.error, console, '%c%s', 'color: green'): log.i;
        log.w = typeof console.warn != 'undefined' ? _.bind(console.warn, console): log.i;
        log.e = typeof console.error != 'undefined' ? _.bind(console.error, console): log.i;
    }

    var lpad2 = function(str) {
        return ('' + str).length < 2 ? '0' + str: str;
    };

    Date.prototype.locale = function() {
        var result = this.toString()
            ,date = ['', '', '', '', '']
            ;

        date[0] = lpad2(this.getDate());
        date[1] = lpad2(this.getMonth() + 1);
        date[2] = this.getFullYear();
        date[3] = lpad2(this.getHours());
        date[4] = lpad2(this.getMinutes());

        switch(LOCALE) {
            case 'ru-ru':
                result = date[0] + '.' + date[1] +  '.' + date[2] + ' ' + date[3] + ':' + date[4];
                break;
            case 'en-en':
                result = date[0] + '.' + date[1] +  '.' + date[2] + ' ' + date[3] + ':' + date[4];
                break;
            case 'en-us':
                result = date[1] + '.' + date[2] +  '.' + date[2] + ' ' + date[3] + ':' + date[4];
                break;
        }

        return result;
    };

//    $.fn.
//
////    '-webkit-', '-moz-', '-o-'

    return '';
});
